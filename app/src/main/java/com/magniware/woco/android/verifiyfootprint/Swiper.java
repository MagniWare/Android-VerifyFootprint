package com.magniware.woco.android.verifiyfootprint;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by Tobi Stockinger on 14/05/14.
 */
public class Swiper extends SwipeRefreshLayout {
	private static final String CLASSTAG = "Swiper";

	public Swiper(Context context) {
		super(context);
	}

	public Swiper(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean canChildScrollUp() {
		boolean canChildScrollUp = super.canChildScrollUp();
		try{
			ListView listView = (ListView) this.findViewById(android.R.id.list);
			canChildScrollUp = listView.getFirstVisiblePosition() != 0;
		}catch (NullPointerException e){
			Log.w(CLASSTAG,"Couln't find ListView to check first visible position");
		}

		return canChildScrollUp;
	}
}
