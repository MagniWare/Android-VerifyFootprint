package com.magniware.woco.android.verifiyfootprint;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

import controllers.FootprintController;
import model.types.LocationUpdate;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    });

    // assuming "this" is an Activity
    FootprintController footprintController = new FootprintController(this);
    if(footprintController.lacksLocationPermissions()) {
      footprintController.requestLocationPermission(this);
    }
    if(footprintController.lacksWritePermissions()) {
      footprintController.requestWriteAccessPermission(this);
    }
    if(!footprintController.hasAllNecessaryPermissions()) {
      footprintController.requestAllNecessaryPermissions(this);
    }

    List<LocationUpdate> testList = FootprintController.getLocationUpdates(this,1000);
    Log.d("MainActivity", "We have " + String.valueOf(testList.size()) + " in the database");

    TextView output = (TextView) this.findViewById(R.id.maintext);
    if( output != null) {
      output.setText("We have " + String.valueOf(testList.size()) + " entries in the database");
    }

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
